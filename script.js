
let testArr = [1, 'two',
   { "three": 3, 4: "four" },
   [5, "six", ['seven', 8]],
   true, null, undefined,];

function filterBy(arr, type) {

   //---method arr.forEach--//
   // let insideArr = [];
   // arr.forEach(element => {
   //    if (typeof element !== type) {
   //       insideArr.push(element)
   //    }
   // });

   //---method arr.filter--//
   let insideArr = arr.filter(function (element) {
      return typeof element !== type;
   });

   return insideArr;
}

//-----TEST-----//
console.log(testArr);
const allTypes = ['string', 'number', 'boolean'];
allTypes.forEach(type => console.log(filterBy(testArr, type)));
